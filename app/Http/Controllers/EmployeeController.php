<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeImportRequest;
use App\Jobs\ImportEmployeesJob;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class EmployeeController extends Controller
{
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }

    public function import(): JsonResponse
    {
        try {
            $request = app(EmployeeImportRequest::class);
        } catch (ValidationException $e) {
            $message = Str::of("NOT ACCEPTABLE: ");

            collect($e->errors())->each(function ($error) use (&$message) {
                $message = $message->append($error[0] . ", ");
            });

            $message = $message->replaceLast(', ', '');

            return response()->json([
                'message' => $message->__toString()
            ], 406);
        }


        $file = $request->file('employees');

        ImportEmployeesJob::dispatch($file->get(), auth()->user());

        return response()->json([
            'message' => __('messages.employee.import.success')
        ]);
    }
}

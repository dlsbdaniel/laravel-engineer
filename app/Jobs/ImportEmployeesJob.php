<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\EmployeeImportNotification;
use App\Services\CsvService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportEmployeesJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected $employeeCsv, protected User $user)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CsvService $service)
    {
        $employees = $service->importEmployeesFromCsv($this->employeeCsv, $this->user);

        $this->user->notify(new EmployeeImportNotification($employees));
    }
}

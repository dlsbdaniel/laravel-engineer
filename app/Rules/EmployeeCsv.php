<?php

namespace App\Rules;

use App\Services\CsvService;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class EmployeeCsv implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $csv = $value->get();

        $records = (new CsvService())->parseFromString($csv);

        $this->validateRecords($records);

        return true;
    }

    protected function validateRecords($records)
    {
        $rules = [
            "*.name" => ['string'],
            "*.email" => ['email'],
            "*.document" => ['numeric'],
            "*.city" => ['string'],
            "*.state" => ['string'],
            "*.start_date" => [new EmployeeStartDate()]
        ];

        $messages = [
            "*.start_date" => "Invalid Date"
        ];

        foreach ($records as $record) {
            $validator = Validator::make([$record], $rules, $messages);
            $validator->validate();
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}

<?php

namespace App\Services;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Collection;
use League\Csv\Reader;
use League\Csv\Statement;

class CsvService
{
    public string $delimiter = ';';

    public function parseFromString(string $csv)
    {
        $reader = Reader::createFromString($csv);
        $reader->setDelimiter($this->delimiter);
        $reader->setHeaderOffset(0);

        $stmt = Statement::create();

        return $stmt->process($reader);
    }

    public function importEmployeesFromCsv(string $csv, User $user = null): Collection
    {
        $records = $this->parseFromString($csv);

        $added = 0;

        $updated = 0;

        $employees = collect();

        foreach ($records as $record) {
            $employee = Employee::where('email', $record['email'])->first() ?? new Employee();
            $employee->fill($record);
            $employee->user()->associate($user);

            $employee->exists ? $updated++ : $added++;

            $employee->save();

            $employees->add($employee);
        }

        return collect([
            'added' => $added,
            'updated' => $updated,
            'employees' => $employees
        ]);
    }
}

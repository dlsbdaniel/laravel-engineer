<?php

namespace Tests\Feature;

use App\Services\CsvService;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\TestCase;

class CsvServiceTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->actingAs($this->user);

        $this->service = app(CsvService::class);

        $this->csv = file_get_contents(__DIR__ . '/../stubs/employees.csv');
    }

    public function testItParsesACsvString()
    {
        $csv = $this->csv;

        $records = $this->service->parseFromString($csv);

        $this->assertCount(4, $records);
    }

    public function testItReturnsACollectionOfEmployeesWhenParsingACsvWithEmployeeData()
    {
        $csv = $this->csv;

        $result = $this->service->importEmployeesFromCsv($csv, $this->user);

        $employees = $result['employees'];

        $this->assertInstanceOf(Collection::class, $employees);
        $this->assertContainsOnlyInstancesOf(Employee::class, $employees);
        $this->assertCount(4, $employees);
    }

    public function testItReturnsHowManyEmployeesWereAddedAndUpdatedWhenParsingACsvWithEmployeeData()
    {
        $csv = $this->csv;

        $result = $this->service->importEmployeesFromCsv($csv, $this->user);

        $employeesAdded = $result['added'];
        $employeesUpdated = $result['updated'];

        $this->assertEquals(4, $employeesAdded);
        $this->assertEquals(0, $employeesUpdated);
    }

    public function testItParsesACsvStringOfEmployeesAndSavesThemToTheDatabase()
    {
        $csv = $this->csv;

        $result = $this->service->importEmployeesFromCsv($csv, $this->user);

        $employees = $result['employees'];

        $this->assertInstanceOf(Collection::class, $employees);
        $this->assertCount(4, $employees);
        $this->assertDatabaseCount((new Employee())->getTable(), 4);

        $employees->each(function (Employee $e) {
            $this->assertEquals($this->user->id, $e->user_id);
        });
    }
}

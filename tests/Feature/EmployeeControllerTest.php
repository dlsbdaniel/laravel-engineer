<?php

namespace Tests\Feature;

use App\Jobs\ImportEmployeesJob;
use App\Models\Employee;
use App\Models\User;
use App\Notifications\EmployeeImportNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Queue;
use Laravel\Passport\Passport;
use Tests\TestCase;

class EmployeeControllerTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function testItGetsAllEmployeesWithAnAuthUser()
    {
        $employees = Employee::factory(3)->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertJson(
                $employees->map(function ($employee) {
                    return [
                        'id' => $employee->id,
                        'user_id' => $employee->user_id,
                        'name' => $employee->name,
                        'email' => $employee->email,
                        'document' => $employee->document,
                        'city' => $employee->city,
                        'state' => $employee->state,
                        'start_date' => $employee->start_date,
                    ];
                })->toArray()
            );
    }

    public function testItCannotGetEmployeesFromAnotherUser()
    {
        $user = User::factory()->create();

        Employee::factory(3)->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertExactJson([]);
    }

    public function testItShowsAnEmployeeWithAnAuthUser()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertOk()
            ->assertJson([
                'id' => $employee->id,
                'user_id' => $employee->user_id,
                'name' => $employee->name,
                'email' => $employee->email,
                'document' => $employee->document,
                'city' => $employee->city,
                'state' => $employee->state,
                'start_date' => $employee->start_date,
            ]);
    }

    public function testItCannotShowAnEmployeeFromAnotherUser()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');
    }

    public function testItDeletesAnEmployeeWithAnAuthUser()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]))
            ->assertOk()
            ->assertExactJson([
                'Success' => true
            ]);

        $this->assertDatabaseMissing('employees', [
            'id' => $employee->id
        ]);
    }

    public function testItCannotDeleteAnEmployeeFromAnotherUser()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);
    }

    public function testItImportsEmployeesFromCsvFile()
    {
        $csv = file_get_contents(__DIR__ . '/../stubs/employees.csv');

        $file = UploadedFile::fake()->createWithContent('employees.csv', $csv);

        $response = $this->post(route('employees.import'), ['employees' => $file]);

        $response->assertSessionDoesntHaveErrors()
                 ->assertStatus(200);

        $this->assertDatabaseCount((new Employee())->getTable(), 4);
    }

    public function testItImportsEmployeesFromCsvFileInAQueue()
    {
        Queue::fake();

        $csv = file_get_contents(__DIR__ . '/../stubs/employees.csv');

        $file = UploadedFile::fake()->createWithContent('employees.csv', $csv);

        $response = $this->post(route('employees.import'), ['employees' => $file]);

        $response->assertSessionDoesntHaveErrors()
                 ->assertStatus(200);

        Queue::assertPushed(ImportEmployeesJob::class);
    }

    public function testItImportsEmployeesFromCsvFileAndNotifiesUser()
    {
        Notification::fake();

        $csv = file_get_contents(__DIR__ . '/../stubs/employees.csv');

        $file = UploadedFile::fake()->createWithContent('employees.csv', $csv);

        $response = $this->post(route('employees.import'), ['employees' => $file]);

        $response->assertSessionDoesntHaveErrors()
                 ->assertStatus(200);

        Notification::assertSentTo($this->user, EmployeeImportNotification::class);
    }


    public function testItImportsExistingEmployeeFromCsvAndUpdatesInTheDatabase()
    {
        $employeesTable = (new Employee())->getTable();

        $importingCsv = file_get_contents(__DIR__ . '/../stubs/employees.csv');

        $updatingCsv = file_get_contents(__DIR__ . '/../stubs/employees_update.csv');

        $file = UploadedFile::fake()->createWithContent('employees.csv', $importingCsv);

        $response = $this->post(route('employees.import'), ['employees' => $file]);

        $response->assertSessionDoesntHaveErrors()
                 ->assertStatus(200);

        $this->assertDatabaseCount($employeesTable, 4);

        $file = UploadedFile::fake()->createWithContent('employees.csv', $updatingCsv);

        $response = $this->post(route('employees.import'), ['employees' => $file]);

        $response->assertSessionDoesntHaveErrors()
                 ->assertStatus(200);

        $this->assertDatabaseCount($employeesTable, 4)
             ->assertDatabaseHas($employeesTable, [
                 'email' => 'marco@kyokugen.org',
                 'user_id' => $this->user->id,
                 'state' => 'SP'
             ]);
    }

    public function testItCannotImportCsvFileContainingInvalidDate()
    {
        $csv = file_get_contents(__DIR__ . '/../stubs/employees_invalid.csv');

        $file = UploadedFile::fake()->createWithContent('employees.csv', $csv);

        $response = $this->post(route('employees.import'), ['employees' => $file]);

        $response->assertStatus(406)->assertJson([
            'message' => "NOT ACCEPTABLE: Invalid Date"
        ]);

        $this->assertDatabaseMissing((new Employee())->getTable(), [
            'name' => 'Jimmy Blanka',
            'email' => 'blanka@enel.com.br',
            'user_id' => $this->user->id,
            'state' => 'AM'
        ]);
    }
}
